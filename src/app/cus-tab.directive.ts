import {ContentChildren, Directive, ElementRef, Input, OnInit, Renderer2} from '@angular/core';
import {
  concatMap,
  distinctUntilChanged,
  fromEvent,
  map,
  Observable,
  of,
  scan,
  take,
  throttleTime,
  timer,
  windowTime
} from "rxjs";


export interface ICollapse {
  open: IView | null;
  close: IView | null;
}

export interface IView {
  button: HTMLElement;
  content: HTMLElement;
  value: string;
}

export interface ICusTabConfig {
  duration: number;
}


@Directive({
  selector: '[appCusTab]'
})
export class CusTabDirective implements OnInit {

  public buttons!: HTMLElement[];
  public contents!: HTMLElement[];
  public elements: IView[] = [];

  // @ContentChildren('')

  @Input('appCusTab')
  configs: ICusTabConfig = {duration: 1000};

  constructor(private elementRef: ElementRef,
              private renderer: Renderer2,
  ) {
  }

  public clickEvent$ = new Observable<Event>();

  ngOnInit(): void {
    this.buttons = [...this.elementRef.nativeElement.querySelectorAll('*[data-cus-toggle]')];
    this.contents = [...this.elementRef.nativeElement.querySelectorAll('*[data-cus-content]')];
    this.elements = [...this.buttons].map((button: HTMLElement): IView => (
        {
          button,
          content: this.contents.find((content: HTMLElement) => (
              content.dataset['cusContent'] === button.dataset['cusToggle']
            )
          ) ?? button,
          value: button.dataset['cusToggle'] ?? ''
        }
      )
    );

    this.contents.forEach((content: HTMLElement) => {
        this.renderer.setStyle(content, 'visibility', 'hidden');
        this.renderer.setStyle(content, 'opacity', '0');
        this.renderer.setStyle(content, 'display', 'none');
        this.renderer.setStyle(content, 'transition', `${(this.configs.duration / 2).toString()}ms`);
      }
    );


    this.clickEvent$ = fromEvent(this.buttons, 'click');
    this.clickEvent$.pipe(
      throttleTime(300),
      map((e: Event): IView => (
          this.elements.find((el) => (
            e.target as HTMLElement).dataset['cusToggle'] === el.value
          ) as IView
        )
      ),
      windowTime(500),
      concatMap(obs => obs.pipe(
          distinctUntilChanged((prev, curr) =>
            prev.value === curr.value
          )
        )
      ),
      scan((acc: ICollapse, curr: IView): ICollapse => (
          curr?.value === acc.open?.value ?
            {close: curr, open: null} :
            {close: acc?.open, open: curr}
        ), {open: null, close: null}
      ),
      concatMap((collapse: ICollapse): Observable<[string, ICollapse]> => (
          timer(0, (this.configs.duration / 2) + 10).pipe(
            map((myNumber): [string, ICollapse] => (
              myNumber % 2 === 0 ?
                ['close', collapse] :
                ['open', collapse]
            )),
            take(2)
          )
        )
      ),
      concatMap((item: [string, ICollapse]): Observable<[string, string, ICollapse]> => (
          item[0] === 'close'
            ?
            timer(0, ((this.configs.duration) / 2)).pipe(
              map((myNumber): [string, string, ICollapse] => (
                myNumber % 2 === 0 ?
                  [item[0], 'animation', item[1]] :
                  [item[0], 'display', item[1]]
              )),
              take(2))
            :
            of([item[0], 'animation', item[1]])
        )
      )
    ).subscribe((item: [string, string, ICollapse]) => {
        if (item[2].open) {
          this.renderer.addClass(item[2].open?.button, 'cus-active-tab');
          this.renderer.setStyle(item[2].open?.button, 'color', 'red');
        }
        if (item[2].close) {
          this.renderer.removeClass(item[2].close?.button, 'cus-active-tab');
          this.renderer.removeStyle(item[2].close?.button, 'color');
        }

        if (item[2].close && item[0] === 'close' && item[1] === 'animation') {
          this.renderer.setStyle(item[2].close?.content, 'visibility', 'hidden');
          this.renderer.setStyle(item[2].close?.content, 'opacity', '0');
        } else if (item[2].close && item[0] === 'close' && item[1] === 'display') {
          this.renderer.setStyle(item[2].close?.content, 'display', 'none');
        } else if (item[2].open && item[0] === 'open' && item[1] === 'animation') {
          this.renderer.setStyle(item[2].open?.content, 'visibility', 'visible');
          this.renderer.setStyle(item[2].open?.content, 'opacity', '1');
        }

        if (item[2].open && item[1] === 'display') {
          if (item[2].close) {
            this.renderer.setStyle(item[2].close?.content, 'display', 'none');
          }
          if (item[2].open) {
            this.renderer.removeStyle(item[2].open?.content, 'display');
          }
        }
      }
    );

  }
}
